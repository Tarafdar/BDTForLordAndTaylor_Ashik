Feature: Create a New Account Functionality

  Background:
    Given Not a validated user
    When  User browse to the site
    Then  Home page should displayed
    And  User handle popup
    When  User click Sign In link


  Scenario: User create a new account


    Then User click Create Account link
    And User enter First Name
    And User enter Last Name
    And User enter Email
    And User enter password
    And User enter Confirm Password
    And User enter Phone Number
    And User enter Postal Code
    And User click Create Account button
    Then User get welcome massage

