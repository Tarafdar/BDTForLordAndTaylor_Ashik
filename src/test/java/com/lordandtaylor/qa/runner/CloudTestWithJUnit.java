package com.lordandtaylor.qa.runner;

import com.lordandtaylor.qa.hook.BeforeCloudTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)

public class CloudTestWithJUnit extends BeforeCloudTest {

    @Test
    public void loginFunctionality(){
        homePage.notAvalidatedUser();
        homePage.userBrowseToTheSite();
        homePage.homePageShoulDisplayed();

        loginPage.userHandlePopup();
        loginPage.userClickSigninLink();
        loginPage.browserShouldDisplaySigninPage();

        loginPage.userEnterEmail();
        loginPage.userEnterPassword();
        loginPage.clickLoginButton();
        homePage.browserShouldDisplayedWithWelcomeMassage();


    }

    @Test
    public void createNewAccount(){

    }


}
