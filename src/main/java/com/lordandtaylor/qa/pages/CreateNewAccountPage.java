package com.lordandtaylor.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class CreateNewAccountPage extends PageBase{

    public void clickCreateANewAccountLink(){
        WebElement createNewAccount = driver.findElement(By.xpath("//*[@class='hbc-button hbc-button--block hbc-button--secondary account-message-card__button']"));

        createNewAccount.click();

        // //*[@class='hbc-button hbc-button--block hbc-button--secondary account-message-card__button']
    }   // //*[contains(@class,'hbc-button--secondary')]

    public void enterFirstName (){
        WebElement firstName = driver.findElement(By.xpath("//input[@id='create-new-account-first-name']"));
        firstName.sendKeys("Md");
    }

    public void enterLastName (){
        WebElement lastName = driver.findElement(By.xpath("//input[@id='create-new-account-last-name']"));
        lastName.sendKeys("Tarafdar");
    }

    public void enterEmail (){
        WebElement email = driver.findElement(By.xpath("//input[@id='create-new-account-email']"));
        email.sendKeys("mar.tarafdar@gmail.com");
    }

    public void enterPassword (){
        WebElement password = driver.findElement(By.xpath("//input[@id='create-new-account-password']"));
        password.sendKeys("Mart1996$");
    }

    public void enterConfirmPassword (){
        WebElement confirmPassword = driver.findElement(By.xpath("//input[@id='create-new-account-confirm-password']"));
        confirmPassword.sendKeys("Mart1996$");
    }

    public void enterPhoneNumber (){
        WebElement phoneNumber = driver.findElement(By.xpath("//input[@id='create-new-account-phone-number']"));
        phoneNumber.sendKeys("3473370108");
    }

    public void enterPostalCode (){
        WebElement postalCode = driver.findElement(By.xpath("//input[@id='create-new-account-postal-code']"));
        postalCode.sendKeys("10462");
    }

    public void clickCreateAccount (){
        WebElement createAccount = driver.findElement(By.xpath("//*[@class='hbc-button hbc-button--full hbc-button--primary']"));
        createAccount.click();
    }


}
