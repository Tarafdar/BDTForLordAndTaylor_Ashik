package com.lordandtaylor.qa.steps;


import com.lordandtaylor.qa.pages.HomePage;
import com.lordandtaylor.qa.pages.VerificationPage;
import cucumber.api.java.en.Then;
import org.junit.Assert;

public class HomePagrSteps extends StepsBase {
    //VerificationPage verificationPage = new VerificationPage();
    HomePage homePage = new HomePage();

    @Then("^Home page should displayed$")
    public void home_page_should_displayed() {
       /* String title = driver.getTitle();
        System.out.println(title);
        Assert.assertEquals("Lord & Taylor: Designer Clothing, Shoes, Handbags, Accessories & More", title); */
       homePage.homePageShoulDisplayed();

    }

    @Then("^Browser should displayed with welcome massage$")
    public void browser_should_displayed_with_welcome_massage() {
       /* String welcomePageTitle = driver.getTitle();
        System.out.println(welcomePageTitle);
        Assert.assertEquals("lordandtaylor.com", welcomePageTitle);*/

       homePage.browserShouldDisplayedWithWelcomeMassage();
    }


}
