package com.lordandtaylor.qa.steps;

import com.lordandtaylor.qa.pages.LoginPage;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class LoginPageSteps extends StepsBase {
    LoginPage loginPage = new LoginPage();

    @Then("^Browser should display Sign In page$")
    public void browser_should_display_Sign_In_page() {
        /* String title = driver.getTitle();
        System.out.println(title);
        Assert.assertEquals("Lordandtaylor.com", title); */

        loginPage.browserShouldDisplaySigninPage();

    }

    @And("^User handle popup$")
    public void User_handle_popup(){
       /* WebElement popupElement = null;
        try{
            popupElement = driver.findElement(By.xpath(".//*[@id ='generic-modal']"));
            System.out.println("Found iframe");

        }catch (Exception ex){
            System.out.println(ex.getMessage());

        }
        if (popupElement != null){
            WebElement popup = popupElement.findElement(By.xpath("./iframe"));
            driver.switchTo().frame(popup);
            System.out.println("Switched to iframe");
            WebElement closeButton = driver.findElement(By.cssSelector("#close-button"));
            closeButton.click();
        } */
       loginPage.userHandlePopup();

    }

    @When("^User click Sign In link$")
    public void User_click_Sign_In_link() {
       /* WebElement element1 = driver.findElement(By.xpath("//a[@class='account']"));
        delayFor(2000);
        element1.click(); */
       loginPage.userClickSigninLink();

    }


    @When("^User enter email as \"([^\"]*)\"$")
    public void user_enter_email_as(String arg1) {
       /* WebElement email = driver.findElement(By.xpath("//input[@id='sign-into-account-email-field']"));
        email.clear();
        email.sendKeys("ashik_mart@yahoo.com"); */
       loginPage.userEnterEmail();

    }

    @When("^User enter password as \"([^\"]*)\"$")
    public void user_enter_password_as(String arg1) {
      loginPage.userEnterPassword();

    }

    @When("^User click Login button$")
    public void user_click_Login_button() {
     loginPage.clickLoginButton();

    }


        /* WebElement welcomeMassage = driver.findElement(By.xpath("//h1[@class='account-header__greeting']"));
        System.out.println(welcomeMassage);
        delayFor(2000);
        String expected = "MD's Account";
        delayFor(2000);
        if(expected.contains("MD's Account")){
            System.out.println("match");
        }else {
            System.out.println("Don't Match"); */

    // Assert.assertEquals(expected, welcomeMassage);

        //  //h1[contains(@class,'account-header__greeting')]
        // css .account-header__greeting


    }


