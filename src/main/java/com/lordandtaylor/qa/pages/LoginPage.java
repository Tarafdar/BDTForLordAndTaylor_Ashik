package com.lordandtaylor.qa.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class LoginPage extends PageBase{

    public void browserShouldDisplaySigninPage(){
        String title = driver.getTitle();
        System.out.println(title);
        Assert.assertEquals("Lordandtaylor.com", title);

    }

    public void userHandlePopup(){
        WebElement popupElement = null;
        try{
            popupElement = driver.findElement(By.xpath(".//*[@id ='generic-modal']"));
            System.out.println("Found iframe");

        }catch (Exception ex){
            System.out.println(ex.getMessage());

        }
        if (popupElement != null){
            WebElement popup = popupElement.findElement(By.xpath("./iframe"));
            driver.switchTo().frame(popup);
            System.out.println("Switched to iframe");
            WebElement closeButton = driver.findElement(By.cssSelector("#close-button"));
            closeButton.click();
        }

    }

    public void userClickSigninLink(){
        WebElement element1 = driver.findElement(By.xpath("//a[@class='account']"));
        delayFor(2000);
        element1.click();

    }

    public void userEnterEmail(){
        WebElement email = driver.findElement(By.xpath("//input[@id='sign-into-account-email-field']"));
        email.clear();
        email.sendKeys("ashik_mart@yahoo.com");

    }

    public void userEnterPassword(){
        WebElement password = driver.findElement(By.xpath("//input[@name='password']"));
        password.clear();
        password.sendKeys("mart1996");
    }
    public void clickLoginButton(){
        WebElement signin = driver.findElement(By.xpath("//button[@type='submit']"));
        signin.click();
    }


}
