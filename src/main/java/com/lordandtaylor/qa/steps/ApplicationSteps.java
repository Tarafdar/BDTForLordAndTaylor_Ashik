package com.lordandtaylor.qa.steps;

import com.lordandtaylor.qa.pages.HomePage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class ApplicationSteps extends StepsBase {
    HomePage homePage = new HomePage();

    @Given("^Not a validated user$")
    public void not_a_validated_user(){
        //driver.manage().deleteAllCookies();
        homePage.notAvalidatedUser();
    }

    @When("^User browse to the site$")
    public void user_browse_to_the_site(){
       // driver.navigate().to("https://www.lordandtaylor.com");
        homePage.userBrowseToTheSite();
    }

}
