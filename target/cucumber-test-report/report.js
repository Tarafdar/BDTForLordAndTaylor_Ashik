$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("CreateNewAccount.feature");
formatter.feature({
  "line": 1,
  "name": "Create a New Account Functionality",
  "description": "",
  "id": "create-a-new-account-functionality",
  "keyword": "Feature"
});
formatter.before({
  "duration": 6617789700,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Not a validated user",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "User browse to the site",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "Home page should displayed",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User handle popup",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "User click Sign In link",
  "keyword": "When "
});
formatter.match({
  "location": "ApplicationSteps.not_a_validated_user()"
});
formatter.result({
  "duration": 196111200,
  "status": "passed"
});
formatter.match({
  "location": "ApplicationSteps.user_browse_to_the_site()"
});
formatter.result({
  "duration": 5444222300,
  "status": "passed"
});
formatter.match({
  "location": "HomePagrSteps.home_page_should_displayed()"
});
formatter.result({
  "duration": 26795700,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageSteps.User_handle_popup()"
});
formatter.result({
  "duration": 353893100,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageSteps.User_click_Sign_In_link()"
});
formatter.result({
  "duration": 5669418900,
  "status": "passed"
});
formatter.scenario({
  "line": 11,
  "name": "User create a new account",
  "description": "",
  "id": "create-a-new-account-functionality;user-create-a-new-account",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 14,
  "name": "User click Create Account link",
  "keyword": "Then "
});
formatter.step({
  "line": 15,
  "name": "User enter First Name",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "User enter Last Name",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "User enter Email",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "User enter password",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "User enter Confirm Password",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "User enter Phone Number",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "User enter Postal Code",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "User click Create Account button",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "User get welcome massage",
  "keyword": "Then "
});
formatter.match({
  "location": "CreateNewAccountSteps.user_click_Create_Account_link()"
});
formatter.result({
  "duration": 180164800,
  "status": "passed"
});
formatter.match({
  "location": "CreateNewAccountSteps.user_enter_First_Name()"
});
formatter.result({
  "duration": 469013400,
  "status": "passed"
});
formatter.match({
  "location": "CreateNewAccountSteps.user_enter_Last_Name()"
});
formatter.result({
  "duration": 190373900,
  "status": "passed"
});
formatter.match({
  "location": "CreateNewAccountSteps.user_enter_Email()"
});
formatter.result({
  "duration": 506352500,
  "status": "passed"
});
formatter.match({
  "location": "CreateNewAccountSteps.user_enter_password()"
});
formatter.result({
  "duration": 169489500,
  "status": "passed"
});
formatter.match({
  "location": "CreateNewAccountSteps.user_enter_Confirm_Password()"
});
formatter.result({
  "duration": 213719800,
  "status": "passed"
});
formatter.match({
  "location": "CreateNewAccountSteps.user_enter_Phone_Number()"
});
formatter.result({
  "duration": 194088400,
  "status": "passed"
});
formatter.match({
  "location": "CreateNewAccountSteps.user_enter_Postal_Code()"
});
formatter.result({
  "duration": 130209600,
  "status": "passed"
});
formatter.match({
  "location": "CreateNewAccountSteps.user_click_Create_Account_button()"
});
formatter.result({
  "duration": 164055900,
  "status": "passed"
});
formatter.match({
  "location": "CreateNewAccountSteps.user_get_welcome_massage()"
});
formatter.result({
  "duration": 27100,
  "status": "passed"
});
formatter.after({
  "duration": 11700,
  "status": "passed"
});
formatter.uri("Login.feature");
formatter.feature({
  "line": 1,
  "name": "Login Functionality",
  "description": "",
  "id": "login-functionality",
  "keyword": "Feature"
});
formatter.before({
  "duration": 32310600,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Not a validated user",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "User browse to the site",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "Home page should displayed",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User handle popup",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "User click Sign In link",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "Browser should display Sign In page",
  "keyword": "Then "
});
formatter.match({
  "location": "ApplicationSteps.not_a_validated_user()"
});
formatter.result({
  "duration": 481216900,
  "status": "passed"
});
formatter.match({
  "location": "ApplicationSteps.user_browse_to_the_site()"
});
formatter.result({
  "duration": 2751716800,
  "status": "passed"
});
formatter.match({
  "location": "HomePagrSteps.home_page_should_displayed()"
});
formatter.result({
  "duration": 910832200,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageSteps.User_handle_popup()"
});
formatter.result({
  "duration": 655005500,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageSteps.User_click_Sign_In_link()"
});
formatter.result({
  "duration": 5388919600,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageSteps.browser_should_display_Sign_In_page()"
});
formatter.result({
  "duration": 14804600,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "1. Sign In with valid user and valid password",
  "description": "",
  "id": "login-functionality;1.-sign-in-with-valid-user-and-valid-password",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 15,
  "name": "User enter email as \"ashik_mart@yahoo.com\"",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "User enter password as \"Mart1996#\"",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "User click Login button",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "Browser should displayed with welcome massage",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "ashik_mart@yahoo.com",
      "offset": 21
    }
  ],
  "location": "LoginPageSteps.user_enter_email_as(String)"
});
formatter.result({
  "duration": 334335800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Mart1996#",
      "offset": 24
    }
  ],
  "location": "LoginPageSteps.user_enter_password_as(String)"
});
formatter.result({
  "duration": 201553700,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageSteps.user_click_Login_button()"
});
formatter.result({
  "duration": 7889325500,
  "status": "passed"
});
formatter.match({
  "location": "HomePagrSteps.browser_should_displayed_with_welcome_massage()"
});
formatter.result({
  "duration": 248811600,
  "status": "passed"
});
formatter.after({
  "duration": 22700,
  "status": "passed"
});
});