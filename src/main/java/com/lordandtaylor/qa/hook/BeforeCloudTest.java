package com.lordandtaylor.qa.hook;

import com.lordandtaylor.qa.pages.HomePage;
import com.lordandtaylor.qa.pages.LoginPage;
import com.lordandtaylor.qa.utils.DriverFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.runners.Parameterized;
import org.openqa.selenium.WebDriver;

import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

public class BeforeCloudTest {

    protected WebDriver driver;

   protected HomePage homePage;
   protected LoginPage loginPage;



    @Parameterized.Parameter()
    public String browserName = "chrome";

    @Before
    public void setUp() throws Exception {

        driver = DriverFactory.getInstance(browserName).getDriver();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(10,TimeUnit.SECONDS);

        driver.manage().window().maximize();


        homePage = new HomePage();
        loginPage = new LoginPage();



    }

    @After
    public void tearDown(){
        homePage = null;
        loginPage = null;

        DriverFactory.getInstance().removeDriver();
    }


    @Parameterized.Parameters(name = "{index} - Browser - {0}")
    public static Collection<Object[]> browsers(){
        return Arrays.asList(new Object[][]{
                {"chrome"},
                //{"firefox"},
              //  {"cloud_chrome_64"},
                //{"cloud_ie_11"}
        });
    }
}
