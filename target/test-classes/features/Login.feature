Feature: Login Functionality

  Background:
    Given Not a validated user
    When  User browse to the site
    Then  Home page should displayed
    And  User handle popup
    When  User click Sign In link
    Then  Browser should display Sign In page


  Scenario: 1. Sign In with valid user and valid password


    When  User enter email as "ashik_mart@yahoo.com"
    And   User enter password as "Mart1996#"
    And   User click Login button
    Then  Browser should displayed with welcome massage

  #Scenario: Sign In with valid email and invalid password


    #When  User enter email as "ashik_mart@yahoo.com"
    #And   User enter password as "Mart1969#"
    #And   User click Login button
    #Then  Home page should displayed invalid user


  #Scenario: Sign In with invalid email and valid password


    #When  User enter invalid email as "ashik_mar@yahoo.com"
    ##And   User click Login button
    #Then  Home page should displayed invalid user

