package com.lordandtaylor.qa.runner;


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)

@CucumberOptions(
        features = {//"src/test/resources/features/CreateNewAccount.feature"},
                 // "src/test/resources/features/Login.feature"},
                   "src/test/resources/features"},

        glue = "com.lordandtaylor.qa.steps",

        //C:\MyDevelopment\SSMB4\BDTForLordAndTaylor\src\main\java\com\lordandtaylor\qa\steps

        dryRun = false,
        //monochrome = true,
        //format = {"html:test-out put"},

        plugin = {
                "pretty:target/cucumber-test-report/cucumber-pretty.txt",
                "html:target/cucumber-test-report",
                "json:target/cucumber-test-report/cucumber-report.json",
                "junit:target/cucumber-test-report/test-report.xml",
                "json:target/test-report.json"
        }

)


public class BDTRunner {
}
