package com.lordandtaylor.qa.steps;

import com.lordandtaylor.qa.pages.CreateNewAccountPage;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class CreateNewAccountSteps extends StepsBase {

    CreateNewAccountPage createNewAccountPage = new CreateNewAccountPage();


   /* @When("^User is already on Home page$")
    public void user_is_already_on_Home_page(){
    driver.navigate().to("https://www.lordandtaylor.com");
    }*/

    @When("^User click Create Account link$")
    public void user_click_Create_Account_link(){
     createNewAccountPage.clickCreateANewAccountLink();
    }

    @Then("^User enter First Name$")
    public void user_enter_First_Name() {
        createNewAccountPage.enterFirstName();
    }

    @Then("^User enter Last Name$")
    public void user_enter_Last_Name() {
        createNewAccountPage.enterLastName();
    }

    @Then("^User enter Email$")
    public void user_enter_Email()  {
        createNewAccountPage.enterEmail();

    }

    @Then("^User enter password$")
    public void user_enter_password() {
        createNewAccountPage.enterPassword();
    }

    @Then("^User enter Confirm Password$")
    public void user_enter_Confirm_Password(){
        createNewAccountPage.enterConfirmPassword();
    }

    @Then("^User enter Phone Number$")
    public void user_enter_Phone_Number() {
        createNewAccountPage.enterPhoneNumber();
    }

    @Then("^User enter Postal Code$")
    public void user_enter_Postal_Code(){
        createNewAccountPage.enterPostalCode();
    }

    @Then("^User click Create Account button$")
    public void user_click_Create_Account_button() {
        createNewAccountPage.clickCreateAccount();
    }

    @Then("^User get welcome massage$")
    public void user_get_welcome_massage()  {

    }



}
