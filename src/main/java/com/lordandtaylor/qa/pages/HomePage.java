package com.lordandtaylor.qa.pages;

import org.junit.Assert;

public class HomePage extends PageBase {

    public void notAvalidatedUser(){
        driver.manage().deleteAllCookies();
    }

    public void userBrowseToTheSite(){
        driver.navigate().to("https://www.lordandtaylor.com");
    }

    public void homePageShoulDisplayed(){
        String title = driver.getTitle();
        System.out.println(title);
        Assert.assertEquals("Lord & Taylor: Designer Clothing, Shoes, Handbags, Accessories & More", title);
    }

    public void browserShouldDisplayedWithWelcomeMassage(){
        String welcomePageTitle = driver.getTitle();
        System.out.println(welcomePageTitle);
        Assert.assertEquals("lordandtaylor.com", welcomePageTitle);
    }


}
