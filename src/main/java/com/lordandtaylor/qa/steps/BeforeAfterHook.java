package com.lordandtaylor.qa.steps;

import com.lordandtaylor.qa.utils.DriverFactory;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class BeforeAfterHook extends StepsBase {
    @Before
    public void setup() {

        //ChromeDriverManager.getInstance().setup();
       // driver = new ChromeDriver();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);

        driver.manage().window().maximize();
    }

    @After
    public void tearDown(){
        //driver.close();
        //driver.quit();
       // delayFor(4000);
        //DriverFactory.getInstance().removeDriver();

    }

}
